File Handler
==============
This extension help to handle file compression and etc..

Option Handler
==============
This extension help to ease the rendering and resolving of options array.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist mfmdevsystem/yii2-lib "dev-master"
```

or add

```
"mfmdevsystem/yii2-lib": "dev-master"
```

to the require section of your `composer.json` file.

Usage for FileHandler
-----
Compress the image to 73px width. The height will auto calculate.
```php
$fileHandler = new \mfmdevsystem\lib\FileHandler($completePath);
$fileHandler->compressImage(73);
```
Compress the image to 80% reduction of original width.
```php
$fileHandler = new \mfmdevsystem\lib\FileHandler($completePath);
$fileHandler->compressImage(null, 80);
```

Usage for OptionHandler
-----
First you need to create a class and extend this widget
```php

namespace common\utilities;

use Yii;

class OptionHandler extends \mfmdevsystem\lib\OptionHandler {

    public static function populate() {
        $data = parent::populate();

        $data['user-status'] = [
            0 => Yii::t('app', 'Blocked'),
            1 => Yii::t('app', 'Active'),
        ];

        $data['user-role'] = [
            'auditor' => Yii::t('app', 'Auditor'),
            'guest' => Yii::t('app', 'Guest'),
        ];

        // Other data options goes here

        return $data;
    }

}

```


To render an option list. Return array
```php
common\utilities\OptionHandler::render($key);
```

To resolve an option code. Return string
```php
common\utilities\OptionHandler::resolve($key, $code);
```