<?php

namespace mfmdevsystem\lib;

use Yii;
use yii\helpers\HtmlPurifier;

/**
 * @author Faeez Mazlan <mfaeezmazlan@gmail.com>
 * @version 1.1.0
 */
class GeneralHelper
{
	/**
	 * Replacing sensitive content (IC number) with ****
	 */
	public static function replaceSensitiveContent($str)
	{
		if ($str && is_string($str)) {
			return preg_replace("/\d{12}|\d{6}\-\d{2}\-\d{4}/", "************", $str);
		}
		return null;
	}

	/**
	 * Remove character starting with '=', '@', '+', '-' and '\t'. '\t' is equal to tab.
	 * Then execute HtmlPurifier.
	 * Return null if $str is not a valid string 
	 * @param string $str The string to be sanitize
	 * @return string|null
	 */
	public static function sanitize($str)
	{
		if ($str && is_string($str)) {
			$output = trim($str);
			$pattern = '/^(\=|\@|\+|\-|\t)*/';
			return HtmlPurifier::process(preg_replace($pattern, '', $output));
		}
		return null;
	}

	/**
	 * Generate random string
	 * @param integer $length The length of the generate string, default to 8
	 * @param boolean $includeSymbols Set true if you want the chances to include symbols in the strings, default to false
	 * @return string
	 */
	public static function generateRandomString($length = 8, $includeSymbols = false)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$symbols = '~!@#$%^&*()_+';
		if ($includeSymbols) $characters .= $symbols;
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	/**
	 * Generate random string for password
	 * @param integer $length The length of the generate string, default to 8
	 * @param boolean $includeSymbols Set true if you want the chances to include symbols in the strings, default to false
	 * @return string
	 */
	public static function generateRandomPassword($length = 8, $includeSymbols = false)
	{
		//define character libraries - remove ambiguous characters like iIl|1 0oO
		$sets = array();
		$sets[] = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
		$sets[] = 'abcdefghjkmnpqrstuvwxyz';
		$sets[] = '23456789';
		if ($includeSymbols) $sets[]  = '~!@#$%^&*(){}[],./?';
		$password = '';
		foreach ($sets as $set) {
			$password .= $set[array_rand(str_split($set))];
		}
		while (strlen($password) < $length) {
			$randomSet = $sets[array_rand($sets)];
			$password .= $randomSet[array_rand(str_split($randomSet))];
		}
		return str_shuffle($password);
	}

	/**
	 * Get the file type based it content type
	 * @param string $contentType Content type in string such as "image/png"
	 * @return string
	 */
	public static function getFileType($contentType)
	{
		$type = 'default';
		$contentType = explode('/', $contentType);

		switch ($contentType[0]) {
			case 'image':
				$type = 'image';
				break;
			case 'application':
				switch ($contentType[1]) {
					case 'pdf':
						$type = 'pdf';
						break;
					case 'msword':
					case 'vnd.openxmlformats-officedocument.wordprocessingml.document':
						$type = 'word';
						break;
					case 'vnd.ms-excel':
					case 'vnd.openxmlformats-officedocument.spreadsheetml.sheet':
						$type = 'excel';
						break;
				}
				break;
		}

		return $type;
	}

	/**
	 * Function to convert human readable bytes to specific bytes.
	 * Eg: 1kB = 1024
	 * @param string $value
	 * @return int 
	 */
	public static function convertBytes($value)
	{
		if (is_numeric($value)) {
			return $value;
		} else {
			$value_length = strlen($value);
			$qty = substr($value, 0, $value_length - 1);
			$unit = strtolower(substr($value, $value_length - 1));
			switch ($unit) {
				case 'k':
					$qty *= 1024;
					break;
				case 'm':
					$qty *= 1048576;
					break;
				case 'g':
					$qty *= 1073741824;
					break;
			}
			return $qty;
		}
	}

	/**
	 * Function to convert bytes to human readable.
	 * Eg: 1024 = 1MB
	 * @param integer $size
	 * @param integer $precision
	 * @return mixed
	 */
	public static function convertHumanBytes($size, $precision = 2)
	{
		$units = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
		$step = 1024;
		$i = 0;
		while (($size / $step) > 0.9) {
			$size = $size / $step;
			$i++;
		}
		return round($size, $precision) . $units[$i];
	}

	/**
	 * Function to dump and execute SQL File.
	 * If there is failure during execution of command, exit() command will be
	 * execute.
	 * @param string $filename The full path of the sql file to be executed.
	 */
	public static function importSqlFile($filename)
	{
		echo "Attempt to import " . $filename . ".\n";
		$db = Yii::$app->db;
		$sql = '';
		$lines = file($filename);
		foreach ($lines as $line) {
			if (substr($line, 0, 2) == '--' || $line == '' || substr($line, 0, 2) == "/*")
				continue;

			$sql .= $line;

			if (substr(trim($line), -1, 1) == ';') {
				echo "Attempt to execute the following command:\n";
				echo $sql . "\n";
				try {
					$command = $db->createCommand($sql);
					$command->execute();
				} catch (\Exception $e) {
					echo $e->getMessage() . "\n";
					exit();
				}
				$sql = '';
			}
		}
		echo "Tables imported successfully.\n";
	}

	/**
	 * Generate abbreviation from given string
	 * @param string $string
	 * @return string
	 */
	public static function generateAbbreviation($string)
	{
		$data = explode(' ', trim($string));
		$output = '';
		foreach ($data as $x) {
			if (strlen($x) > 0) {
				$output .= $x[0];
			}
		}
		$output = self::clean($output);

		return $output;
	}

	/**
	 * Attempt to strip spaces and remove special characters
	 * @param string $string
	 * @param boolean $replaceSpaceWithHyphens  Default to false
	 * @return string
	 */
	public static function clean($string, $replaceSpaceWithHyphens = false)
	{
		if ($replaceSpaceWithHyphens) {
			$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		}
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}

	/**
	 * Attempt to strip spaces and remove special characters then append '+'
	 * @param string $string
	 * @return string
	 */
	public static function formatPhoneNo($string)
	{
		$output = preg_replace('/[^A-Za-z0-9]/', '', $string);
		return '+' . $output;
	}

	public static function generalNumberReading($n)
	{
		if ($n < 1000000) {
			// Anything less than a million
			$n_format = Yii::$app->formatter->asDecimal($n, 2);
		} else if ($n < 1000000000) {
			// Anything less than a billion
			$n_format = Yii::$app->formatter->asDecimal($n / 1000000, 2) . 'M';
		} else {
			// At least a billion
			$n_format = Yii::$app->formatter->asDecimal($n / 1000000000, 2) . 'B';
		}
		return $n_format;
	}

	/**
	 * Usually use to display html caret of increase and decreasing percentage.
	 * @param mixed $val
	 * @return string 
	 */
	public static function htmlCaret($val)
	{
		$output = null;
		if ($val > 0) $output = '<span class="text-green"><i class="fa fa-caret-up"></i> ' . number_format($val, 0) . '%</span>';
		elseif ($val < 0) $output = '<span class="text-red"><i class="fa fa-caret-down"></i> ' . number_format($val, 0) . '%</span>';
		else $output = '<span class="text-yellow"><i class="fa fa-caret-left"></i> ' . number_format($val, 0) . '%</span>';
		return $output;
	}

	/**
	 * Check if URL is http or https
	 * @return bool 
	 */
	public static function isHttps()
	{
		if (
			isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
			isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
			$_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'
		) {
			return true;
		}
		return false;
	}

	/**
	 * Removing bom characters ï»¿
	 * @param $str string
	 * @return string
	 */
	public static function removeBomUtf8($str)
	{
		$bom = pack('H*', 'EFBBBF');
		$str = preg_replace("/^$bom/", '', $str);
		if (substr(bin2hex($str), 0, 6) === 'efbbbf') $str = substr($str, 3);
		return $str;
	}
}
