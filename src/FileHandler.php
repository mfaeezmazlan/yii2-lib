<?php

namespace mfmdevsystem\lib;

use Yii;

/**
 * @author Faeez Mazlan <mfaeezmazlan@gmail.com>
 * @version 1.1.0
 */
class FileHandler
{
    public $src;
    public $image;
    public $extension;
    public $listExtensionAllow = [
        'png', 'PNG',
        'jpg', 'jpeg', 'JPG', 'JPEG',
        'gif', 'GIF',
    ];

    /**
     * Constructor of FileHandler class
     * @param string $src Complete file path to be convert in the system
     * @throws \Exception
     */
    public function __construct($src)
    {
        if (file_exists($src)) {
            $extension = substr($src, strrpos($src, '.') + 1);
            if (in_array($extension, $this->listExtensionAllow)) {
                $this->src = $src;
                $this->extension = $extension;
            } else {
                throw new \Exception('File given is not an image extension allowed. Only image with extension ' . implode(', ', $this->listExtensionAllow) . ' area allowed.<br>Path given: ' . $this->src, 404);
            }
        } else {
            throw new \Exception('File is not exist.<br>Path given: ' . $src, 404);
        }
    }

    /**
     * Compress image from saved file in the system.
     * @param integer $targetWidth default to null and will use reductionPercentage to calculate the result of width
     * @param integer $reductionPercentage default 50%
     * @throws \Exception
     */
    public function compressImage($targetWidth = null, $reductionPercentage = 50)
    {
        $this->createImage();

        list($originalWidth, $originalHeight) = getimagesize($this->src);

        $resultWidth = $targetWidth;
        if (!$resultWidth) {
            $resultWidth = $originalWidth - ($originalWidth * $reductionPercentage / 100);
        }

        $resultHeight = ($originalHeight / $originalWidth) * $resultWidth;

        $tmpMin = imagecreatetruecolor($resultWidth, $resultHeight);
        if (in_array($this->extension, ['png', 'gif'])) {
            imagealphablending($tmpMin, false);
            imagesavealpha($tmpMin, true);
        }
        imagecopyresampled($tmpMin, $this->image, 0, 0, 0, 0, $resultWidth, $resultHeight, $originalWidth, $originalHeight);

        $this->outputImage($tmpMin);
    }

    /**
     * Resize an image
     * @param number $destWidth
     * @param number $destHeight
     * @param integer $quality
     * @return boolean
     * @throws \Exception
     */
    public function resize($destWidth, $destHeight, $quality = 100)
    {
        $this->createImage();

        $dest = imagecreatetruecolor($destWidth, $destHeight);

        list($srcWidth, $srcHeight) = getimagesize($this->src);

        $cropWidth = $srcHeight * $destWidth / $destHeight;
        $cropHeight = $srcWidth * $destHeight / $destWidth;

        if ($cropWidth > $srcWidth) {
            $destY = (($srcHeight - $cropHeight) / 2);

            if (in_array($this->extension, ['png', 'gif'])) {
                imagealphablending($dest, false);
                imagesavealpha($dest, true);
            }

            imagecopyresampled($dest, $this->image, 0, 0, 0, $destY, $destWidth, $destHeight, $srcWidth, $cropHeight);
        } else {
            $destX = (($srcWidth - $cropWidth) / 2);

            if (in_array($this->extension, ['png', 'gif'])) {
                imagealphablending($dest, false);
                imagesavealpha($dest, true);
            }

            imagecopyresampled($dest, $this->image, 0, 0, $destX, 0, $destWidth, $destHeight, $cropWidth, $srcHeight);
        }

        $this->outputImage($dest);
    }

    /**
     * Create an image for attribubute imcage from source given based on image extension
     */
    public function createImage()
    {
        switch ($this->extension) {
            case 'png':
            case 'PNG':
                $this->image = imagecreatefrompng($this->src);
                break;
            case 'gif':
            case 'GIF':
                $this->image = imagecreatefromgif($this->src);
                break;
            case 'jpg':
            case 'JPG':
            case 'jpeg':
            case 'JPEG':
                $this->image = imagecreatefromjpeg($this->src);
        }
    }

    /**
     * Output an image
     * @param type $trueColorImage
     */
    public function outputImage($trueColorImage)
    {
        switch ($this->extension) {
            case 'png':
            case 'PNG':
                imagepng($trueColorImage, $this->src, 8);
                break;
            case 'gif':
            case 'GIF':
                imagegif($trueColorImage, $this->src);
                break;
            case 'jpg':
            case 'JPG':
            case 'jpeg':
            case 'JPEG':
                imagejpeg($trueColorImage, $this->src, 80);
        }
    }
}
