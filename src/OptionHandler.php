<?php

namespace mfmdevsystem\lib;

use Yii;

/**
 * @author Faeez Mazlan <mfaeezmazlan@gmail.com>
 * @version 1.1.0
 */
class OptionHandler
{
    public static function populate()
    {
        $data = [];

        return $data;
    }

    public static function render($key)
    {
        $data = static::populate();
        return $data[$key];
    }

    public static function resolve($key, $code)
    {
        $data = static::populate();
        if (isset($data[$key]) && isset($data[$key][$code])) {
            return $data[$key][$code];
        }

        return null;
    }
}
