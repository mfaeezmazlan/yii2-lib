<?php

namespace mfmdevsystem\lib;

use Yii;

/**
 * @author Faeez Mazlan <mfaeezmazlan@gmail.com>
 * @version 1.1.0
 */
class EncryptionHelper
{
    /**
     * Encrypt the content.
     * @param string, $cipher, string $key, string $content
     */
    public static function encrypt($cipher, $key, $content)
    {
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);

        $ciphertext_raw = openssl_encrypt($content, $cipher, $key, OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, $key, true);
        return base64_encode($iv . $hmac . $ciphertext_raw);
    }

    /**
     * Decrypt the content.
     * @param string, $cipher, string $key, string $content
     */
    public static function decrypt($cipher, $key, $content)
    {
        $sha2len = 32;
        $ciphertext = base64_decode($content);

        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = substr($ciphertext, 0, $ivlen);
        $hmac = substr($ciphertext, $ivlen, $sha2len);
        $ciphertext_raw = substr($ciphertext, $ivlen + $sha2len);

        $plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, true);

        if (self::hash_equals($hmac, $calcmac))
            return $plaintext;
        else
            return null;
    }

    // Support for PHP < 5.6
    public static function hash_equals($str1, $str2)
    {
        if (strlen($str1) != strlen($str2)) {
            return false;
        } else {
            $res = $str1 ^ $str2;
            $ret = 0;
            for ($i = strlen($res) - 1; $i >= 0; $i--) {
                $ret |= ord($res[$i]);
            }
            return !$ret;
        }
    }
}
