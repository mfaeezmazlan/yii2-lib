<?php

namespace mfmdevsystem\lib;

use Yii;

/**
 * @author Faeez Mazlan <mfaeezmazlan@gmail.com>
 * @version 1.1.0
 */
class ImageHelper
{
    /**
     * Get image resource based on filetype
     * @return resource
     */
    public static function getImageResource($type, $filepath)
    {
        if (preg_match('/jpg|jpeg/i', $type)) {
            $resource = imagecreatefromjpeg($filepath);
        } elseif (preg_match('/png/i', $type)) {
            $resource = imagecreatefrompng($filepath);
        } elseif (preg_match('/gif/i', $type)) {
            $resource = imagecreatefromgif($filepath);
        } elseif (preg_match('/bmp/i', $type)) {
            $resource = imagecreatefrombmp($filepath);
        } else return false;

        return $resource;
    }

    /**
     * Convert image resource to JPEG
     * Quality range: 0 to 100
     */
    public static function toJpeg($src, $dest, $quality = 75)
    {
        if ($resource = self::getImageResource($src->type, $src->tempName)) {
            $ext = pathinfo($src->name, PATHINFO_EXTENSION);

            if (in_array($ext, ['png', 'gif'])) {
                list($width, $height) = getimagesize($src->tempName);
                $canvas = imagecreatetruecolor($width, $height);
                imagefill($canvas, 0, 0, imagecolorallocate($canvas, 255, 255, 255));
                imagecopyresampled($canvas, $resource, 0, 0, 0, 0, $width, $height, $width, $height);

                imagejpeg($canvas, $dest, $quality);
                imagedestroy($canvas);
            } else {
                imagejpeg($resource, $dest, $quality);
            }

            imagedestroy($resource);
            return true;
        }

        return false;
    }

    /**
     * Convert image resource to PNG
     * Quality range: 0 to 9
     */
    public static function toPng($src, $dest, $quality = 6)
    {
        if ($resource = self::getImageResource($src->type, $src->tempName)) {
            imagealphablending($resource, false);
            imagesavealpha($resource, true);

            imagepng($resource, $dest, $quality);
            imagedestroy($resource);

            return true;
        }

        return false;
    }

    /**
     * Resize image
     */
    public static function resize($filepath, $destWidth, $destHeight, $quality = 100)
    {
        $ext = pathinfo($filepath, PATHINFO_EXTENSION);
        $src = self::getImageResource($ext, $filepath);
        if (!$src) return false;

        $dest = imagecreatetruecolor($destWidth, $destHeight);
        if (in_array($ext, ['png', 'gif'])) {
            imagealphablending($dest, false);
            imagesavealpha($dest, true);
        }

        list($srcWidth, $srcHeight) = getimagesize($filepath);
        $cropWidth = $srcHeight * $destWidth / $destHeight;
        $cropHeight = $srcWidth * $destHeight / $destWidth;

        if ($cropWidth > $srcWidth) {
            $destY = (($srcHeight - $cropHeight) / 2);
            imagecopyresampled($dest, $src, 0, 0, 0, $destY, $destWidth, $destHeight, $srcWidth, $cropHeight);
        } else {
            $destX = (($srcWidth - $cropWidth) / 2);
            imagecopyresampled($dest, $src, 0, 0, $destX, 0, $destWidth, $destHeight, $cropWidth, $srcHeight);
        }

        if (in_array($ext, ['jpg', 'jpeg'])) {
            imagejpeg($dest, $filepath, $quality);
        } elseif ($ext == 'png') {
            $quality = $quality / 10;
            if ($quality >= 10) $quality = 9;
            imagepng($dest, $filepath, $quality);
        } else return false;

        imagedestroy($dest);

        return true;
    }
}
