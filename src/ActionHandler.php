<?php

namespace mfmdevsystem\lib;

use Yii;
use yii\helpers\Url;

/**
 * @author Faeez Mazlan <mfaeezmazlan@gmail.com>
 * @version 1.1.0
 *
 * Handles action from /widgets/ActionBar
 * Includes predefined attributes
 * Support single or multiple primary keys
 */
class ActionHandler
{
    public $primaryKey;
    public $route;
    public $action;
    public $hasDelete = false;

    public $model;
    public $selections = [];
    public $actions = [];

    public $triggerEvent = true;

    public function registerTranslations()
    {
        $i18n = Yii::$app->i18n;
        $i18n->translations['widgets/actionbar'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@vendor/mfmdevsystem/widgets/ActionBar/messages',
            'fileMap' => [
                'widgets/actionbar' => 'actionbar.php',
            ],
        ];
    }

    public static function t($message, $params = [], $language = null)
    {
        return Yii::t('widgets/actionbar', $message, $params, $language);
    }

    /**
     * Populate default actions
     */
    public function populate()
    {
        $action['new'] = function () {
            return $this->redirect(['/' . $this->route . '/create']);
        };

        $action['edit'] = function () {
            $this->editItem();
        };

        $action['cancel'] = function () {
            return $this->goBack(['/' . $this->route]);
        };
        $action['close'] = $action['cancel'];
        $action['back'] = $action['cancel'];

        $action['delete'] = function () {
            $count = $this->deleteItem();
            if ($count) $this->setFlash('success', static::t('{count} item deleted', ['count' => $count]) . '.');
            if ($this->model->{$this->primaryKey[0]}) return $this->goBack(['/' . $this->route]);
        };

        $this->actions = $action;
    }

    function __construct($model, $actions = [])
    {
        $this->registerTranslations();

        $this->populate();
        if ($actions) $this->setAction($actions);

        $this->model = $model;
        $this->primaryKey = $model->tableSchema->primaryKey;
        if (!$this->route) $this->route = dirname(Yii::$app->controller->route);
        if ($model->hasAttribute('is_deleted')) $this->hasDelete = true;

        if (Yii::$app->request->post('action')) {
            $this->action = key(Yii::$app->request->post('action'));
            $this->selections = \yii\helpers\Json::decode(Yii::$app->request->post('actionbar_selection'));
        }

        // For single record via model id, insert into selections
        if (!$this->selections) $this->setModelAsSelection($this->model);
    }

    public function setAction($actions)
    {
        $this->actions = array_merge($this->actions, $actions);
        return $this;
    }

    public function execute()
    {
        if (array_key_exists($this->action, $this->actions)) {
            return call_user_func($this->actions[$this->action], $this->model, $this->selections, $this);
        }

        return $this->action;
    }

    /**
     * Perform built in actions
     */
    public function editItem()
    {
        $params = $this->generateQueryFromPk();
        $options = ['/' . $this->route . '/update'];
        $options += $params;
        $url = Url::to($options);

        return $this->redirect($url);
    }

    public function deleteItem()
    {
        if ($this->hasDelete) $count = $this->updateAll(['is_deleted' => 1]);
        else $count = $this->deleteAll();
        return $count;
    }

    public function publishItem()
    {
        $attr = [];
        if ($this->model->hasAttribute('published_at')) {
            $attr['published_at'] = date('U');
        }

        if ($this->model->hasAttribute('published_by') && !Yii::$app->user->isGuest) {
            $attr['published_by'] = Yii::$app->user->id;
        }

        $count = $this->updateAll(array_merge($attr, ['is_published' => 1]));

        return $count;
    }

    public function unpublishItem()
    {
        $count = $this->updateAll(['is_published' => 0]);
        return $count;
    }

    /**
     * Getter, setter and utility tools
     */
    public function updateAll($attributes, $condition = [])
    {
        $count = 0;
        $model = $this->model;
        if (count($this->primaryKey) > 1) {
            foreach ($this->selections as $selection) {
                $selection += $condition;
                if ($this->triggerEvent) {
                    $items = $model::findAll($this->makeCondition($selection));
                    foreach ($items as $item) {
                        $item->setAttributes($attributes, false);
                        if ($item->save(false)) $count++;
                    }
                } else {
                    $count += $model::updateAll($attributes, $this->makeCondition($selection));
                }
            }
        } else {
            if ($this->triggerEvent) {
                $items = $model::findAll($this->makeCondition($condition));
                foreach ($items as $item) {
                    $item->setAttributes($attributes, false);
                    if ($item->save(false)) $count++;
                }
            } else {
                $count = $model::updateAll($attributes, $this->makeCondition($condition));
            }
        }

        return $count;
    }

    public function deleteAll($condition = [])
    {
        $count = 0;
        $model = $this->model;
        if (count($this->primaryKey) > 1) {
            foreach ($this->selections as $selection) {
                $selection += $condition;
                if ($this->triggerEvent) {
                    $items = $model::findAll($this->makeCondition($selection));
                    foreach ($items as $item) {
                        if ($item->delete() !== false) $count++;
                    }
                } else {
                    $count += $model::deleteAll($this->makeCondition($selection));
                }
            }
        } else {
            if ($this->triggerEvent) {
                $items = $model::findAll($this->makeCondition($condition));
                foreach ($items as $item) {
                    if ($item->delete() !== false) $count++;
                }
            } else {
                $count = $model::deleteAll($this->makeCondition($condition));
            }
        }

        return $count;
    }

    public function setFlash($type = 'success', $message)
    {
        if ($type == 'success') $status = '<b>' . static::t('Success') . ':</b>';
        elseif ($type == 'error') $status = '<b>' . static::t('Error') . ':</b>';
        else $status = null;

        if ($status) $message = $status . ' ' . $message;
        Yii::$app->getSession()->setFlash($type, $message);
    }

    protected function makeCondition($criteria = [])
    {
        $condition = [];
        if (count($this->primaryKey) == 1) {
            $condition += [$this->primaryKey[0] => $this->selections];
        }

        $condition += $criteria;
        if ($this->hasDelete && !isset($params['is_deleted'])) {
            $condition += ['is_deleted' => 0];
        }

        return $condition;
    }

    protected function setModelAsSelection($model)
    {
        $primaryKey = $model->tableSchema->primaryKey;
        if (isset($model->{$primaryKey[0]}) && $model->{$primaryKey[0]}) {
            if (count($primaryKey) > 1) {
                $selection = [];
                foreach ($primaryKey as $pk) {
                    $selection += [$pk => $model->{$pk}];
                }
                $this->selections = [$selection];
            } else {
                $this->selections = [$model->{$primaryKey[0]}];
            }
        }
    }

    protected function generateQueryFromPk()
    {
        $params = [];
        if ($this->selections) {
            if (count($this->primaryKey) > 1) {
                foreach ($this->selections[0] as $pk => $val) {
                    $params += [$pk => $val];
                }
            } else {
                $params += ['id' => $this->selections[0]];
            }
        }

        return $params;
    }

    // Implements redirect and memory for return url
    public static function getAlias()
    {
        return Yii::$app->controller->module->id . '/' . Yii::$app->controller->id;
    }

    public static function setReturnUrl()
    {
        // Strip query parameter from url
        $url = Yii::$app->request->url;
        $parsedUrl = parse_url($url);
        if (isset($parsedUrl['path'], $parsedUrl['query'])) {
            parse_str($parsedUrl['query'], $query);
            unset($query['_pjax']);
            $url = $parsedUrl['path'] . '?' . http_build_query($query);
        }

        Url::remember($url, static::getAlias());
    }

    public function redirect($url)
    {
        Yii::$app->controller->redirect($url);
        Yii::$app->end();
    }

    public function goBack($defaultUrl = null)
    {
        $url = (Url::previous(static::getAlias())) ?: $defaultUrl;
        if ($url) $this->redirect($url);
    }

    public function gotoReturnUrl($model, $action = null)
    {
        if (!$action) $action = $this->action;

        switch ($action) {
            case 'save':
            case 'update':
                $this->setModelAsSelection($model);

                $params = $this->generateQueryFromPk();
                $options = ['/' . $this->route . '/update'];
                $options += $params;

                return $this->redirect(Url::to($options));
                break;

            case 'save2close':
            case 'close':
                return $this->goBack(['/' . $this->route]);
                break;

            case 'save2new':
            case 'create':
                return $this->redirect(['/' . $this->route . '/create']);
                break;
        }
    }
}
