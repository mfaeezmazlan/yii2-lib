<?php

namespace mfmdevsystem\widgets;

use yii\web\AssetBundle;

/**
 * @author Faeez Mazlan <mfaeezmazlan@gmail.com>
 * @version 1.1.0
 */
class GridNavAsset extends AssetBundle
{
    public $sourcePath = '@vendor/mfmdevsystem/yii2-lib/widgets/assets';
    public $css = [
        'css/gridnav.css',
    ];
    public $js = [
        'js/param.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
