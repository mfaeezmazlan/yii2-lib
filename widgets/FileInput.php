<?php

namespace mfmdevsystem\widgets;

use Yii;
use mfmdevsystem\lib\Html;

/**
 * @author Faeez Mazlan <mfaeezmazlan@gmail.com>
 * @version 1.1.0
 */
class FileInput extends \yii\widgets\InputWidget
{
    public $clientOptions = [];

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();

        echo Html::activeFileInput($this->model, $this->attribute, $this->options, $this->clientOptions);
    }
}
