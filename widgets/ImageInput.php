<?php

namespace mfmdevsystem\widgets;

use Yii;
use mfmdevsystem\lib\Html;

/**
 * @author Faeez Mazlan <mfaeezmazlan@gmail.com>
 * @version 1.1.0
 */
class ImageInput extends \yii\widgets\InputWidget
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();

        echo Html::activeImageInput($this->model, $this->attribute, $this->options);
    }
}
