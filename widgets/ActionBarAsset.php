<?php

namespace mfmdevsystem\widgets;

use yii\web\AssetBundle;

/**
 * @author Faeez Mazlan <mfaeezmazlan@gmail.com>
 * @version 1.1.0
 */
class ActionBarAsset extends AssetBundle
{
    public $sourcePath = '@vendor/mfmdevsystem/yii2-lib/widgets/assets';
    public $css = [
        'css/actionbar.css',
        'css/color.css',
    ];
    public $cssOptions = ['position' => \yii\web\View::POS_END];
    public $js = [];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
