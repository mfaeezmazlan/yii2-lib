<?php

namespace common\widgets;

use Yii;
use yii\helpers\Html;

/**
 * @author Faeez Mazlan <mfaeezmazlan@gmail.com>
 * @version 1.1.0
 *
 * Renders checkbox tree view from array source
 */
class ArrayTreeView extends \yii\bootstrap\Widget
{
    public $models;
    public $options;
    public $checkAll = false;
    public $disabled = false;
    public $include = [];
    public $exclude = [];

    public function init()
    {
        parent::init();

        Yii::$app->view->registerCss("
            .treeview {padding:10px 15px;border:1px solid #ccc;overflow-y:auto;}
            .treeview .trigger {font-size:16px;margin-right:10px;cursor:pointer;}
            .treeview input[type=checkbox] {margin-right:5px;}
            .treeview > div div {margin-left:2em;}

            .treeview .trigger i {width:10px;}
            .treeview .trigger > .disabled {color:#666;cursor:default;}
            .treeview .node {position:relative;left:-4px;color:#428bca;}
            .treeview .folder {border-left:1px dotted #ccc;}
            .treeview .item {position:relative;left:15px;color:#666;}
            .treeview .highlight {color:#d9534f;}
        ");

        Yii::$app->view->registerJs("
            // Make contans search case-insensitive
            jQuery.expr[':'].contains = jQuery.expr.createPseudo(function(arg) {
                return function(el) {
                    return jQuery(el).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
                };
            });

            $('.treeview [id^=toggle-]').click(function() {
                var id = $(this).attr('id').replace('toggle-', '');

                if ($('#folder-'+id).is(':visible')) $(this).html(getCaret(false));
                else $(this).html(getCaret(true));

                $('#folder-'+id).slideToggle();
            });

            $('.treeview [id^=node-]').click(function() {
                var id = $(this).attr('id').replace('node-', '');
                $('#folder-'+id+' :checkbox').prop('checked', $(this).is(':checked'));

                toggleParents(id);
            });

            $('.treeview .item :checkbox').click(function() {
                var id = $(this).parents().eq(2).attr('id').replace('folder-', '');

                // Process immediate parent folder
                toggleParentCheckbox(id);

                toggleParents(id);
            });

            function toggleParents(id) {
                $('#folder-'+id).parentsUntil('.treeview').each(function() {
                    if ($(this).attr('id')) {
                        var id = $(this).attr('id').replace('folder-', '');
                        toggleParentCheckbox(id);
                    }
                });
            }

            function toggleParentCheckbox(id) {
                var el = $('#node-'+id);
                if ($('#folder-'+id+' :checkbox:checked').length == $('#folder-'+id+' :checkbox').length) {
                    el.prop('checked', true);
                } else {
                    el.prop('checked', false);
                }
            }

            function getCaret(isExpanded) {
                if (isExpanded) return '<i class=\"fa fa-caret-down\"></i>';
                else return '<i class=\"fa fa-caret-right\"></i>';
            }

            $(document).ready(function() {
                $($('.treeview .item :checkbox').get().reverse()).each(function() {
                    var id = $(this).parents().eq(2).attr('id').replace('folder-', '');
                    toggleParentCheckbox(id);
                    toggleParents(id);
                });
            });

            // Search input events
            var timer;
            var searchInput = $('.treeview #search');
            searchInput.on('keyup', function() {
                clearTimeout(timer);
                timer = setTimeout(search, 500);
            });

            searchInput.on('keydown', function() {
                clearTimeout(timer);
            });

            function search() {
                var keyword = $('.treeview #search').val();
                $('.treeview label').removeClass('highlight');
                if (keyword) {
                    var result = $('.treeview label:contains(\"'+ keyword +'\")');
                    result.addClass('highlight');
                
                    // Expand all result entry, collapse others
                    if (result.length) {
                        $('.treeview .folder:has(.highlight)').show();
                        $('.treeview .folder:not(:has(.highlight))').hide();
                    }
                } else {
                    $('.treeview .folder').show();
                }

                // Toggle all carets
                $('.treeview .folder').each(function() {
                    var id = $(this).attr('id').replace('folder-', '');
                    var el = $('#toggle-'+id);
                    if ($('#folder-'+id).is(':visible')) el.html(getCaret(true));
                    else el.html(getCaret(false));
                });
            }
        ", \yii\web\View::POS_END);
    }

    public function run()
    {
        $options = ['class' => 'treeview'];
        if (isset($this->options['height'])) {
            $options += ['style' => 'height:' . $this->options['height'] . 'px'];
        }

        echo Html::beginTag('div', $options);
        $placeholder = (isset($this->options['placeholder'])) ? $this->options['placeholder'] : Yii::t('app', 'Search');
        echo Html::textInput('search', null, [
            'placeholder' => $placeholder, 'autocomplete' => 'off',
            'id' => 'search', 'class' => 'form-control', 'style' => 'margin-bottom:15px;'
        ]);

        $this->traverseNode($this->models);
        echo Html::endTag('div');
    }

    public function traverseNode($models)
    {
        foreach ($models as $model) {
            if (isset($model['items'])) {
                if (count($model['items'])) {
                    $icon['caret'] = Html::tag('span', '<i class="fa fa-caret-down"></i>', ['id' => 'toggle-' . $model['id'], 'class' => 'trigger']);
                } else {
                    $icon['caret'] = Html::tag('span', '<i class="fa fa-caret-right disabled"></i>', ['class' => 'trigger']);
                }

                $checkbox['node'] = Html::checkbox('node[' . $model['id'] . ']', $this->checkAll, [
                    'label' => '<i class="fa fa-folder-open"></i> ' . $model['label'], 'id' => 'node-' . $model['id'],
                    'disabled' => $this->disabled,
                ]);
                echo Html::tag('div', $icon['caret'] . $checkbox['node'], ['class' => 'node']);
            } else {
                $isChecked = $this->checkAll;
                if (in_array($model['id'], $this->include)) $isChecked = true;
                if (in_array($model['id'], $this->exclude)) $isChecked = false;

                $checkbox['item'] = Html::checkbox('item[' . $model['id'] . ']', $isChecked, [
                    'label' => '<i class="fa fa-file"></i> ' . $model['label'], 'uncheck' => 0, 'id' => 'item-' . $model['id'],
                    'disabled' => $this->disabled,
                ]);
                echo Html::tag('div', $checkbox['item'], ['class' => 'item']);
            }

            // Traverse sub-folders
            if (isset($model['items'])) {
                echo Html::beginTag('div', ['id' => 'folder-' . $model['id'], 'class' => 'folder']);
                $this->traverseNode($model['items']);
                echo Html::endTag('div');
            }
        }
    }
}
