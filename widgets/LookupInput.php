<?php

namespace mfmdevsystem\widgets;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;

/**
 * @author Faeez Mazlan <mfaeezmazlan@gmail.com>
 * @version 1.1.0
 */
class LookupInput extends \yii\widgets\InputWidget
{
    public $url;
    public $initValueText = null;
    public $height = '300px';

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();

        return $this->renderWidget();
    }

    /**
     * Renders the widget.
     */
    private function renderWidget()
    {
        $name = isset($options['name']) ? $options['name'] : Html::getInputName($this->model, $this->attribute);
        $value = isset($options['value']) ? $options['value'] : Html::getAttributeValue($this->model, $this->attribute);

        $options = $this->options;
        if (!array_key_exists('id', $this->options)) {
            $options['id'] = Html::getInputId($this->model, $this->attribute);
        }

        $this->renderModal($options['id']);
        return $this->lookupInput($name, $value, $options);
    }

    /**
     * Renders the lookup input.
     */
    private function lookupInput($name, $value = null, $options = [])
    {
        $container_id = $options['id'] . '-container';
        $selection_id = $options['id'] . '-selection';
        if (!isset($options['disabled'])) $options['disabled'] = false;

        Yii::$app->view->registerCss("
            #" . $selection_id . ":disabled {
                background-color:#eee;
                cursor:not-allowed;
            }
        ");

        Yii::$app->view->registerJs("
            $('#" . $selection_id . "').on('click', function() {
                lookup.open($(this).attr('id').replace('-selection', ''));
            });

            $('#" . $selection_id . "').on('focus', function(e) { 
                e.preventDefault();
                $(this).blur();
            });
        ", \yii\web\View::POS_END);


        $btn['lookup'] = Html::button('<i class="glyphicon glyphicon-search"></i>', [
            'class' => 'btn btn-primary',
            'onclick' => "lookup.open('" . $options['id'] . "')",
            'disabled' => $options['disabled'],
        ]);

        $btn['remove'] = Html::button('<i class="glyphicon glyphicon-remove"></i>', [
            'class' => 'btn btn-default',
            'onclick' => "lookup.clear('" . $options['id'] . "')",
            'disabled' => $options['disabled'],
        ]);

        $html = Html::input('hidden', $name, $value, ['id' => $options['id']]);
        if (!$value) $this->initValueText = null;

        $options['id'] = $selection_id;
        $options = array_merge(['class' => 'form-control'], $options);
        $input = Html::input('text', 'lookup-' . $name, $this->initValueText, $options);
        $input .= Html::tag('span', $btn['remove'] . $btn['lookup'], ['class' => 'input-group-btn']);

        $html .= Html::tag('div', $input, ['id' => $container_id, 'class' => 'input-group']);
        return $html;
    }

    /**
     * Renders the lookup modal.
     */
    private function renderModal($id)
    {
        Yii::$app->view->registerJs("
            var lookup = {
                open: function(id) {
                    $('#lookup_modal-'+id).modal('show');
                },

                close: function(id) {
                    $('#lookup_modal-'+id).modal('hide');
                },

                select: function(id, val, text) {
                    $('#'+id).val(val).trigger('change');
                    $('#'+id+'-selection').val(text);
                    lookup.close(id);
                },

                clear: function(id) {
                    $('#'+id).val('').trigger('change');
                    $('#'+id+'-selection').val('');
                },

                enable: function(id) {
                    $('#'+id+'-container input,#'+id+'-container button').prop('disabled', false);
                },

                disable: function(id) {
                    $('#'+id+'-container input,#'+id+'-container button').prop('disabled', true);
                },
            }
        ", \yii\web\View::POS_END, 'lookup-input');

        Modal::begin([
            'id' => 'lookup_modal-' . $id,
            'header' => '<b style="font-size:16px;">Lookup</b>',
        ]);

        echo '<iframe id="lookup_frame-' . $id . '" src="' . Url::to($this->url) . '" style="border:none;width:100%;height:' . $this->height . ';"></iframe>';

        Modal::end();
    }
}
