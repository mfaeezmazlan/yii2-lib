<?php

namespace mfmdevsystem\widgets;

use Yii;
use mfmdevsystem\lib\Html;

/**
 * @author Faeez Mazlan <mfaeezmazlan@gmail.com>
 * @version 1.1.0
 */
class GeneratePasswordInput extends \yii\widgets\InputWidget
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();

        echo Html::activeGeneratePasswordInput($this->model, $this->attribute, $this->options);
    }
}
