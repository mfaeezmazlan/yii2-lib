<?php

namespace mfmdevsystem\widgets;

use Yii;
use yii\helpers\Html;
use yii\base\InvalidConfigException;

/**
 * @author Faeez Mazlan <mfaeezmazlan@gmail.com>
 * @version 1.1.0
 *
 * @dependencies: kartik-v/yii2-icons
 * Renders the page navigations for GridView.
 */
class GridNav extends \yii\bootstrap\Widget
{
    public $dataProvider;
    public $output;

    private $currPage, $lastPage;
    private $pager, $perPage, $extender;

    public $layout = null;
    public $model = null;
    public $addon = [];
    public $options = [];

    public function registerTranslations()
    {
        $i18n = Yii::$app->i18n;
        $i18n->translations['widgets/gridnav'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@vendor/mfmdevsystem/widgets/messages',
            'fileMap' => [
                'widgets/gridnav' => 'gridnav.php',
            ],
        ];
    }

    public static function t($message, $params = [], $language = null)
    {
        return Yii::t('widgets/gridnav', $message, $params, $language);
    }

    public function init()
    {
        $this->registerTranslations();
        GridNavAsset::register($this->getView(), \yii\web\View::POS_END);

        if (!$this->layout) $this->setDefaultLayout();

        // Populate sections
        $this->pager = $this->makePager();
        $this->perPage = $this->makePerPage();
        if (isset($this->addon['extender'])) {
            if ($this->model) $this->extender = $this->makeExtender();
            else throw new InvalidConfigException('The "model" property must be set.');
        }

        $this->getView()->registerJs("
            $('#per-page').change(function() {
                location.href = updateGetParam($(location).attr('href'), 'per-page', $('#per-page').val());
            });

            $(document).on('keydown', '.nav-input', function(e) {
                if (e.which === 13) {
                    var page = $(this).val().replace(/[^0-9]/g, '');
                    location.href = updateGetParam($(location).attr('href'), 'page', page);
                }
            });

            $('#nav-extender-submit').click(function() {
                var columns = [];
                $.each($('#nav-extender input[type=checkbox]:checked'), function() {
                    columns.push($(this).val());
                });

                location.href = updateGetParam($(location).attr('href'), 'column', columns.join(','));
            });
        ", yii\web\View::POS_END, 'gridnav');
    }

    public function run()
    {
        $html = $this->makeNav();
        $html .= '<div class="gridnav-container">' . $this->output . '</div>';
        $html .= $this->makeNav(false);

        echo $html;
    }

    /**
     * Set default layout based on addon parameters
     */
    private function setDefaultLayout()
    {
        if (isset($this->addon['extender'])) {
            $layout = '<div class="pull-left">{pager}</div>';
            $layout .= '<div class="pull-right">';
            $layout .= '<div class="pull-right" style="margin-left:5px;">{per-page}</div><div class="pull-right">{extender}</div>';
            $layout .= '</div>';

            $this->layout = $layout;
        } else {
            $this->layout = '<div class="pull-left">{pager}</div><div class="pull-right">{per-page}</div>';
        }
    }

    /**
     * Render the navigation bar based on layout
     */
    private function makeNav($isPrimary = true)
    {
        $regex = '/{(.*?)}/';
        preg_match_all($regex, $this->layout, $matches);
        foreach ($matches[1] as $key) {
            if ($key == 'pager') $section[$key] = $this->pager;

            if ($isPrimary) {
                if ($key == 'per-page') $section[$key] = $this->perPage;
                elseif ($key == 'extender') $section[$key] = $this->extender;
            }
        }

        if ($this->dataProvider->getPagination() === false) {
            unset($section['per-page']);
        }

        // Map and replace layout with actual sections
        $html = preg_replace_callback($regex, function ($match) use ($section) {
            if (isset($section[$match[1]])) return $section[$match[1]];
        }, $this->layout);

        $html .= '<div class="clearfix"></div>';
        return '<div class="gridnav">' . $html . '</div>';
    }

    /**
     * Render the pager section
     */
    private function makePager()
    {
        $list = [];
        $pagination = $this->dataProvider->getPagination();

        if ($pagination === false) {
            $this->currPage = 0;
            $this->lastPage = 1;
        } else {
            $this->currPage = $pagination->getPage();
            $this->lastPage = $pagination->getPageCount();
        }

        $total = '[ ' . static::t('Total <b>{total}</b> records', ['total' => $this->dataProvider->getTotalCount()]) . ' ]';
        $input = Html::textInput('page', $this->currPage + 1, [
            'class' => 'pull-left nav-input',
        ]);

        $list[] = $this->makePagerLink($pagination, 'first');
        $list[] = $this->makePagerLink($pagination, 'prev');
        $list[] = '<li>' . $input . '</li>';
        $list[] = $this->makePagerLink($pagination, 'next');
        $list[] = $this->makePagerLink($pagination, 'last');

        $pager = '<ul class="pagination" style="margin:0;">' . implode('', $list) . '</ul>';

        $html = '<div class="pull-left">' . static::t('Page') . '</div>';
        $html .= '<div class="pull-left" style="margin:0 5px;">' . $pager . '</div>';
        $html .= '<div class="pull-left">' . static::t('of {total} pages', ['total' => $this->lastPage]) . '</div>';
        $html .= '<div class="pull-left" style="margin-top:3px;margin-left:5px;">' . $total . '</div>';
        $html .= '<div class="clearfix"></div>';

        return '<div class="nav-pager">' . $html . '</div>';
    }

    /**
     * Render pagination and navigation buttons
     */
    private function makePagerLink($pagination, $class)
    {
        if ($pagination !== false) $links = $pagination->getLinks();

        switch ($class) {
            default:
            case 'first':
                $icon = 'angle-double-left';
                $page = 0;
                break;
            case 'prev':
                $icon = 'angle-left';
                $page = $this->currPage - 1;
                break;
            case 'next':
                $icon = 'angle-right';
                $page = $this->currPage + 1;
                break;
            case 'last':
                $icon = 'angle-double-right';
                $page = $this->lastPage - 1;
                break;
        }

        if (isset($links[$class])) {
            return '<li class="' . $class . '">' . Html::a('<i class="fa fa-' . $icon . '"></i>', $links[$class], ['data-page' => $page]) . '</li>';
        } else {
            return '<li class="' . $class . ' disabled"><span><i class="fa fa-' . $icon . '"></i></span></li>';
        }
    }

    /**
     * Render the per-page section
     */
    private function makePerPage()
    {
        $options = [5, 10, 15, 20, 25, 30, 50];
        $option['page-limit'] = array_combine($options, $options);

        // Add custom page limit (if applicable)
        $perPage = Yii::$app->request->get('per-page', '20');
        if (!in_array($perPage, $options) && (intval($perPage) > 0)) {
            $option['page-limit'][$perPage] = $perPage;
            asort($option['page-limit']);
        }

        return Html::dropDownList('per-page', $perPage, $option['page-limit'], [
            'id' => 'per-page', 'class' => 'form-control', ' style' => 'width:60px;height:24px;',
        ]);
    }

    /**
     * Render the extended attributes section
     */
    private function makeExtender()
    {
        $extender = $this->addon['extender'];
        if (!isset($extender['attributes'])) $extender['attributes'] = [];
        if (!isset($extender['visible'])) $extender['visible'] = [];

        if (count($extender['attributes'])) {
            $toggle = Html::a('<i class="fa fa-th-large"></i> Extended Attributes', 'javascript:;', [
                'data-toggle' => 'dropdown',
                'class' => 'dropdown-toggle btn btn-default btn-xs',
                'style' => 'padding-top:2px;height:24px',
            ]);

            $form = '<form id="nav-extender" class="dropdown-menu" style="padding:5px;"><div>';
            foreach ($extender['attributes'] as $attribute => $label) {
                if (is_int($attribute)) {
                    $attribute = $label;
                    $label = $this->model->getAttributeLabel($attribute);
                }

                $checkbox = Html::checkbox('column[]', in_array($attribute, $extender['visible']), [
                    'label' => $label, 'labelOptions' => ['style' => 'font-weight:normal'],
                    'value' => $attribute,
                ]);
                $form .= '<div>' . $checkbox . '</div>';
            }

            $button = Html::a(static::t('Update'), 'javascript:void(0)', [
                'id' => 'nav-extender-submit',
                'class' => 'btn btn-primary btn-xs',
                'style' => 'width:100%',
            ]);

            $form .= '</div>';
            $form .= '<div>' . $button . '</div>';
            $form .= '</form>';

            return '<div class="dropdown">' . $toggle . $form . '</div>';
        }
    }

    /**
     * Format options for extender addon
     */
    public static function formatExtender($namespace, $attributes, $visible, $useCookie = false)
    {
        $column = Yii::$app->request->get('column');

        if ($useCookie) {
            $cookie = Yii::$app->request->cookies->get('gridnav-extender');
            $data = json_decode($cookie, true);

            // Clear cookie for different user login
            if (!Yii::$app->user->isGuest) {
                if (isset($data['user_id']) && $data['user_id'] != Yii::$app->user->id) {
                    Yii::$app->response->cookies->remove('gridnav-extender');
                    $data = ['user_id' => Yii::$app->user->id];
                }
            }

            if (!is_null($column)) {
                $visible = explode(',', $column);
                if (count($visible)) $data[$namespace] = $visible;
                else unset($data[$namespace]);
            } else {
                if (!is_null($data) && isset($data[$namespace])) {
                    $visible = $data[$namespace];
                }
            }

            // Write to cookie (extends expiry)
            $data['stamp'] = uniqid();
            Yii::$app->response->cookies->add(new \yii\web\Cookie([
                'name' => 'gridnav-extender',
                'value' => json_encode($data),
                'expire' => strtotime('+7 days'),
            ]));
        } else {
            if (!is_null($column)) {
                $visible = explode(',', $column);
            }
        }

        return ['attributes' => $attributes, 'visible' => $visible];
    }
}
